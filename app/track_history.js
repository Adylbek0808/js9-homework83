const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const User = require('../models/User');

const router = express.Router();

router.post('/', async (req, res) => {
    const token = req.get('Authorization');
    if (!token) {
        return res.status(401).send({ error: "No token present" })
    }

    const user = await User.findOne({ token })
    if (!user) {
        return res.status(401).send({ error: "Wrong token" })
    }

    try {
        const trackData = req.body;
        trackData.user = user._id;
        trackData.datetime = new Date().toISOString();
        const track = new TrackHistory(trackData);
        await track.save();
        return res.send({ username: user.username, trackData })
    } catch (e) {
        return res.status(400).send(e)
    }

})

module.exports = router;